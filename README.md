# Laravel Interview Question 

The purpose of this assignment is to determine how much your understand Laravel's subtleties and ability to write elegant code.

## The Challenge

Create an authenticated API that supports a user CRUD. 

#### The API

The API should include the following endpoints:

- Lists all users: GET */users*      
- Gets a user resource: GET */users/{user}*
- Creates a user resource: POST */users*
- Updates a user resource: PUT */users/{user}*
- Deletes a user resource: DELETE */users/{user}*

The API must also be authenticated in any way you like, but it must be **STATELESS**.

How you structure your routes file, handle validation and model updating will be very important. Every decision that was taken or lack thereof will be factored. 

Please structure everything in controllers.

#### The User

Eloquent should be used for everything.

The user table looks as follows:

- email => string
- password => string (hashed + salted) 
- name => string
- encoded_id => string (unique identifier for resource)
- ** User timestamps and soft deletes **

Please make sure your model file is fully featured, including casts, dynamic scopes, and observers if needed.

#### The Database

MySQL should be used as the database, and migrations should be written.